package main

import (
	"fmt"
	"log"
	"net/url"
	"sort"
	"strings"
	"sync"
	"time"

	"encoding/json"
)

// Actor manages by workers
type Actor struct {
	wg *sync.WaitGroup

	resp chan *Response
	done chan struct{}

	numbers   []int
	urls      []string
	validUrls map[string]string

	timeout time.Duration
}

// NewActor creates and returns initialized instance
// of Actor
func newActor() *Actor {
	return &Actor{
		wg:        &sync.WaitGroup{},
		validUrls: make(map[string]string),
		timeout:   time.Duration(470 * time.Millisecond),
	}
}

// launches chain of workers
func (a *Actor) run() {
	// initialize response channel
	a.resp = make(chan *Response, len(a.validUrls))

	defer close(a.resp)

	// initialize stop channel
	a.done = make(chan struct{}, 1)

	// clean numbers state
	a.flushNumbers()

	a.wg.Add(len(a.validUrls))
	for _, url := range a.validUrls {
		worker := newWorker(url, a.wg, a.timeout, a.resp)
		go worker.run()
	}

	go func() {
		// deferred closing control channel
		defer close(a.done)
		// wait all routines stopped
		a.wg.Wait()
		// stop selecting from data channel
		a.done <- struct{}{}
	}()

	for {
		select {
		case raw := <-a.resp:
			// handling data In-Time of worker's actions
			nums := &struct{ Numbers []int }{}
			if err := json.Unmarshal(raw.data, nums); err != nil {
				log.Printf("Unexpected data.")
			}
			a.numbers = append(a.numbers, nums.Numbers...)
			// remove duplicates
			deduplicate(&a.numbers)
			// sort slice in Ascend
			sort.Ints(a.numbers)
		case <-a.done:
			return
		}
	}
}

// validates a requested urls
func (a *Actor) validateURLs() {
	for _, u := range a.urls {
		p, err := url.Parse(u)
		if err != nil {
			log.Print(fmt.Sprintf("URL %s is not valid", u))
			continue
		}

		if p.Scheme != "http" {
			fmt.Sprintf("URL has an invalid scheme '%s'", p.Scheme)
			continue
		}

		if p.Host == "" || strings.IndexRune(p.Host, '\\') > 0 {
			fmt.Sprintf("URL has an invalid host '%s'", p.Host)
			continue
		}

		a.validUrls[u] = u
	}
}

func (a *Actor) flushNumbers() {
	a.numbers = []int{}
}
