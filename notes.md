travel audience Go challenge
============================

Design
-------
### Actor
Plays a major role in control by workers in a concurrent way. An Actor instance initialized by once and lives all service uptime. It controls all execution. It is calling immediately after Service was Requested.

Some actions that can consume a time:

- parsing incoming data
- creating numbers of workers
- worker request <-> response

The attached file torch.svg is a Flame Graph that's an analyzed output from pprof tool.
There we can see that most of the time spent falls on calling networking labrary functions.


It is rational to use any data manipulation(if any data already accessed) in time when workers still acting.

	


* Each url spawns its own worker (goroutine): each separate worker request host independently.

### Worker
Is a simple goroutine which spawns by calling `newWorker` function.

* It processes only one url recieved upon initialization
* It do not handles HTTP connection or response errors, stops when
  some errors found
* Just writes raw response body to data channel
* Has a timeout on HTTP connection (~471ms)
* Works as quick as possible


###Test

go test -run=x -bench=. -benchtime=10s