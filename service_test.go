package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"time"
)

const URL = "http://localhost:8080/numbers?u=http://localhost:8090/primes&u=http://localhost:8090/fibo&u=http://localhost:8090/rand&u=http://localhost:8090/odd"

func TestHandleNumbers(t *testing.T) {
	req := makeRequest(t, URL)
	client := &http.Client{
		Timeout: time.Duration(500 * time.Millisecond),
	}

	res, err := client.Do(req)

	defer res.Body.Close()

	if res.StatusCode != 200 {
		t.Error(fmt.Printf("StatusCode: %d\n", res.StatusCode))
	}

	if err != nil {
		t.Error(fmt.Printf("Error: %s\n", err.Error()))
	}
}

func BenchmarkHandleNumbers(b *testing.B) {
	r := makeRequest(b, URL)
	a := newActor()
	for i := 0; i < b.N; i++ {
		w := httptest.NewRecorder()
		handlerNumbers(w, r, a)
	}
}

func makeRequest(t testing.TB, url string) *http.Request {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	return req
}
