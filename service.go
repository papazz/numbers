package main

import (
	"encoding/json"
	"log"
	"net/http"
	_ "net/http/pprof"
)

func handlerNumbers(w http.ResponseWriter, r *http.Request, a *Actor) {
	if r.Method != "GET" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	a.urls = r.Form["u"]
	a.validateURLs()
	a.run()
	res, _ := json.Marshal(struct{ Numbers []int }{Numbers: a.numbers})
	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}

func main() {
	http.HandleFunc("/numbers", func(w http.ResponseWriter, r *http.Request) {
		handlerNumbers(w, r, newActor())
	})
	log.Print("Service Numbers started.")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
