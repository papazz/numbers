package main

import (
	"io/ioutil"
	"net/http"
	"sync"
	"time"
)

// type of transfer
type Response struct {
	url  string
	data []byte
}

// Worker executes url request
type Worker struct {
	url      string
	response chan<- *Response
	wg       *sync.WaitGroup
	timeout  time.Duration
}

// run starts worker
func (w *Worker) run() {
	defer w.wg.Done()
	w.fetch()
}

// recieving data from url
func (w *Worker) fetch() {
	req, err := http.NewRequest("GET", w.url, nil)
	req.Header.Set("Accept", "appllication/json")
	if err != nil {
		return
	}

	client := &http.Client{
		Timeout: w.timeout,
	}
	res, err := client.Do(req)
	if err != nil {
		return
	}
	// read response
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	w.response <- &Response{
		url:  w.url,
		data: body,
	}
}

func newWorker(url string, wg *sync.WaitGroup, t time.Duration, resp chan<- *Response) *Worker {
	return &Worker{
		url:      url,
		wg:       wg,
		timeout:  t,
		response: resp,
	}
}
