package main

func deduplicate(slice *[]int) {
	seen := make(map[int]bool)
	j := 0
	for i, el := range *slice {
		if !seen[el] {
			seen[el] = true
			(*slice)[j] = (*slice)[i]
			j++
		}
	}
	*slice = (*slice)[:j]
}
